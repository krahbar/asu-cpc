/*
ID: pcmonk1
LANG: C
PROG: beads
*/
#include <stdio.h>

char RED = 'r', BLUE = 'b', WHITE = 'w';
int MAXBEADS = 350;

int max(int a, int b) { return (a > b ? a : b); }
int min(int a, int b) { return (a < b ? a : b); }
int longest_backward_chain(char* str, int i, char color, int n);
int longest_forward_chain(char* str, int i, char color, int n);

main()
{
	FILE *fin  = fopen("beads.in", "r");
	FILE *fout = fopen("beads.out", "w");

	int n = 0;
	char beads[MAXBEADS];

	fscanf(fin, "%d\n%s\n", &n, beads);

	int i, possible_beads, max_beads = 0;
	for (i = 0; i < n; i++)
	{
		possible_beads = min(n, max(longest_backward_chain(beads, i, RED, n), longest_backward_chain(beads, i, BLUE, n))
				+ max(longest_forward_chain(beads, i+1, RED, n),longest_forward_chain(beads, i+1, BLUE, n)));
		max_beads = max(possible_beads, max_beads);
	}

	fprintf(fout, "%d\n", max_beads);

	exit(0);
}

int longest_backward_chain(char* str, int i, char color, int n)
{
	int j;
	for (j = 0; (str[(i-j) % n] == color || str[(i-j) % n] == WHITE) && j < n; j++);
	return j;
}

int longest_forward_chain(char* str, int i, char color, int n)
{
	int j;
	for (j = 0; (str[(i+j) % n] == color || str[(i+j) % n] == WHITE) && j < n; j++);
	return j;
}

