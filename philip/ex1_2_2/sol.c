/*
ID: pcmonk1
LANG: C
PROG: transform
*/
#include <stdio.h>
#include <stdlib.h>

int rot90(char ** matrix, char ** transformed, int n);
int rot180(char ** matrix, char ** transformed, int n);
int rot270(char ** matrix, char ** transformed, int n);
int reflect(char ** matrix, char ** transformed, int n);
int combo(char ** matrix, char ** transformed, int n);
int equal(char ** matrix, char ** transformed, int n);

main()
{
	FILE *fin  = fopen("transform.in", "r");
	FILE *fout = fopen("transform.out", "w");

	int n;
	char ** matrix;
	char ** transformed;

	fscanf(fin, "%d\n", &n);

	matrix = malloc(n*sizeof(char*));
	transformed = malloc(n*sizeof(char*));

	int i;
	for (i = 0; i < n; i++)
	{
		matrix[i] = malloc((n+1)*sizeof(char));
		transformed[i] = malloc((n+1)*sizeof(char));
	}

	for (i = 0; i < n; i++)
		fscanf(fin, "%s\n", matrix[i]);
	for (i = 0; i < n; i++)
	{
		fscanf(fin, "%s\n", transformed[i]);
	}

	if (rot90(matrix,transformed,n))
		fprintf(fout,"%d\n",1);
	else if (rot180(matrix,transformed,n))
		fprintf(fout,"%d\n",2);
	else if (rot270(matrix,transformed,n))
		fprintf(fout,"%d\n",3);
	else if (reflect(matrix,transformed,n))
		fprintf(fout,"%d\n",4);
	else if (combo(matrix,transformed,n))
		fprintf(fout,"%d\n",5);
	else if (equal(matrix,transformed,n))
		fprintf(fout,"%d\n",6);
	else
		fprintf(fout,"%d\n",7);

	exit(0);
}

int rot90(char ** matrix, char ** transformed, int n)
{
	int row,col;
	for (row = 0; row < n; row++)
		for (col = 0; col < n; col++)
			if (transformed[col][n-row-1] != matrix[row][col])
				return 0;

	return 1;
}

int rot180(char ** matrix, char ** transformed, int n)
{
	int row,col;
	for (row = 0; row < n; row++)
		for (col = 0; col < n; col++)
			if (transformed[n-row-1][n-col-1] != matrix[row][col])
				return 0;

	return 1;
}

int rot270(char ** matrix, char ** transformed, int n)
{
	int row,col;
	for (row = 0; row < n; row++)
		for (col = 0; col < n; col++)
			if (transformed[n-col-1][row] != matrix[row][col])
				return 0;

	return 1;
}

int reflect(char ** matrix, char ** transformed, int n)
{
	int row,col;
	for (row = 0; row < n; row++)
		for (col = 0; col < n; col++)
			if (transformed[row][n-col-1] != matrix[row][col])
				return 0;

	return 1;
}

int combo(char ** matrix, char ** transformed, int n)
{
	char ** reflected = malloc(n*sizeof(char*));
	
	int i;
	for (i = 0; i < n; i++)
		reflected[i] = malloc((n+1)*sizeof(char));

	int row,col;
	for (row = 0; row < n; row++)
		for (col = 0; col < n; col++)
			reflected[row][n-col-1] = matrix[row][col];

	return rot90(reflected,transformed,n) || rot180(reflected,transformed,n) || rot270(reflected,transformed,n);
}

int equal(char ** matrix, char ** transformed, int n)
{
	int i,j;
	for (i = 0; i < n; i++)
		for (j = 0; j < n; j++)
			if (transformed[i][j] != matrix[i][j])
				return 0;

	return 1;
}

