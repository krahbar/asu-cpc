/*
ID: pcmonk1
LANG: C
PROG: ride
*/
#include <stdio.h>

int name_to_num(char* str);

main()
{
	FILE *fin  = fopen ("ride.in", "r");
	FILE *fout = fopen ("ride.out", "w");
	char comet[6];
	char group[6];
	fscanf(fin, "%s\n%s\n", comet, group);
	fprintf(fout, "%s\n", (name_to_num(comet) == name_to_num(group)) ? "GO" : "STAY");
	exit(0);
}

int name_to_num(char* str)
{
	int num = 1;

	int i = 0;
	for (i = 0; i < 6 && str[i] != 0; i++)
		num *= str[i] - 'A' + 1;

	printf("%s %d\n", str, num);

	return num % 47;
}

