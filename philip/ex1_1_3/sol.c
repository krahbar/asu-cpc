/*
ID: pcmonk1
LANG: C
PROG: gift1
*/
#include <stdio.h>
#include <strings.h>

int MAXNP = 10;
int MAXCHARS = 14;

main()
{
	FILE *fin  = fopen("gift1.in", "r");
	FILE *fout = fopen("gift1.out", "w");
	int np = 0;
	char persons[MAXNP][MAXCHARS];
	int moneys[MAXNP];

	int i;
	for (i = 0; i < MAXNP; i++)
		moneys[i] = 0;

	fscanf(fin, "%d\n", &np);

	for (i = 0; i < np; i++)
		fscanf(fin, "%s\n", persons[i]);

	char temp[MAXNP][MAXCHARS];
	int money = 0, ng_i = 0;
	for (i = 0; 1; i++)
	{
		fscanf(fin, "%s\n", temp[0]);

		if (feof(fin))
			break;

		int n;
		for (n = 0; n < np && strcmp(temp[0],persons[n]); n++);

		fscanf(fin, "%d %d\n", &money, &ng_i);

		if (ng_i == 0)
			continue;

		int j;
		for (j = 0; j < ng_i; j++)
			fscanf(fin, "%s", temp[j]);

		int to_each = money / ng_i;

		int k;
		for (j = 0; j < ng_i; j++)
			for (k = 0; k < np; k++)
				if (!strcmp(temp[j],persons[k]))
					moneys[k] += to_each;

		moneys[n] -= to_each * ng_i;
	}

	for (i = 0; i < np; i++)
		fprintf(fout, "%s %d\n", persons[i], moneys[i]);

	exit(0);
}

