/*
ID: pcmonk1
LANG: C
PROG: friday
*/
#include <stdio.h>
#include <strings.h>

main()
{
	FILE *fin  = fopen("friday.in", "r");
	FILE *fout = fopen("friday.out", "w");

	int n = 0, day_of_last_thirteenth = 4;
	int thirteens[7] = {0,0,0,0,0,0,0};
	int months[12] = {31,31,28,31,30,31,30,31,31,30,31,30};

	fscanf(fin, "%d\n", &n);

	int year, month;
	for (year = 0; year < n; year++)
		for (month = 0; month < 12; month++)
		{
			day_of_last_thirteenth = (day_of_last_thirteenth + ((month == 2 && year % 4 == 0 && (year % 100 != 0 || (year + 300) % 400 == 0)) ? 29 : months[month])) % 7;
			(thirteens[day_of_last_thirteenth])++;
		}

	fprintf(fout, "%d %d %d %d %d %d %d\n", thirteens[0], thirteens[1], thirteens[2], thirteens[3], thirteens[4], thirteens[5], thirteens[6]);

	exit(0);
}

