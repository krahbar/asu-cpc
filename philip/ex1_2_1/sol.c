/*
ID: pcmonk1
LANG: C
PROG: milk2
*/
#include <stdio.h>
#include <stdlib.h>

int MAXN = 5000, MAXTIME = 1000000;

int max(int a, int b) { return (a > b ? a : b); }
int min(int a, int b) { return (a < b ? a : b); }
int in(int time, int * times, int n);

main()
{
	FILE *fin  = fopen("milk2.in", "r");
	FILE *fout = fopen("milk2.out", "w");

	int n;
	int * times;

	fscanf(fin, "%d\n", &n);

	times = malloc(n*2*sizeof(int));

	int i;
	for (i = 0; i < n; i++)
		fscanf(fin, "%d %d\n", &times[2*i], &times[2*i+1]);

	int mintime = MAXTIME, maxtime = 0;
	for (i = 0; i < n; i++)
	{
		mintime = min(mintime, times[2*i]);
		maxtime = max(maxtime, times[2*i+1]);
	}

	int currenton = 0, currentoff = 0, maxon = 0, maxoff = 0;
	for (i = mintime; i < maxtime; i++)
	{
		if (in(i,times,n))
		{
			currenton++;
			maxon = max(maxon,currenton);
			currentoff = 0;
		}
		else
		{
			currenton = 0;
			currentoff++;
			maxoff = max(maxoff,currentoff);
		}
	}

	fprintf(fout, "%d %d\n", maxon, maxoff);

	exit(0);
}

int in(int time, int * times, int n)
{
	int * i;
	for (i = times; i < times + 2 * n; i += 2)
		if (*i <= time && time < *(i+1))
			return 1;

	return 0;
}

